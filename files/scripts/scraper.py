#! /usr/bin/env python3

import scrapy
import w3lib.html
from scrapy.spiders import CrawlSpider, Rule
from scrapy.crawler import CrawlerProcess
from scrapy.linkextractors import LinkExtractor
from w3lib.html import remove_tags
import time
import json, operator
import os
import re
import lxml.html.clean as clean

# setup the HTML cleaner (removes useless or naughty code from the scraped content to make it pretty)

safe_attrs = set(['src', 'alt', 'href', 'title', 'width', 'height'])
kill_tags = ['object', 'iframe', 'script']
cleaner = clean.Cleaner(safe_attrs_only=True, safe_attrs=safe_attrs, kill_tags=kill_tags)

# setup timestamp - if we want to know when something was scraped
ts = time.gmtime()

class PiSpider(CrawlSpider):
    name = "pivilion"
    custom_settings = {
        'DEPTH_LIMIT': '1',

    }

    #The master URL where all other URLs are located

    start_urls = [

          'http://localhost/links',

    ]
     
    # rules for link extraction - list of urls allowed do be scraped (escape all special characters with "\")
    # the callback is the name of the function that gets called when this URL is scraped
    

    rules = (
       Rule(LinkExtractor(allow=('pivilion\.net\/read-me', )), callback='pivilion'),
       Rule(LinkExtractor(allow=('hacklab01\.org\/medialab', )), callback='medialab'),
       Rule(LinkExtractor(allow=('schloss-post\.com\/pivillion-your-personal-darknet-pocket-gallery', )), callback='schlosspost'),
       Rule(LinkExtractor(allow=('kayserworks\.com\/\#\/798817030644', )), callback='solarsinter'),
       Rule(LinkExtractor(allow=('www\.evanmeaney\.com\/_recpi.html', )), callback='crd'),
       Rule(LinkExtractor(allow=('manual\.bibliotecha\.info', )), callback='bibliotecha'),
       Rule(LinkExtractor(allow=('github\.com\/huertanix\/pi2pi', )), callback='pi2pi'),
       Rule(LinkExtractor(allow=('collapseos\.org' )), callback='collapseos'),
       Rule(LinkExtractor(allow=('arambartholl\.com\/dead-drops\/')), callback='deaddrops'),
       Rule(LinkExtractor(allow=('back7\.co\/post\/137926305194\/raspberry-pi-field-unit-rpfu')), callback='rpfu'),
       Rule(LinkExtractor(allow=('sivazona\.hr\/events\/towar-kuu-uzgon')), callback='towar'),
       Rule(LinkExtractor(allow=('tokyotypedirectorsclub\.org\/en\/award\/2020_rgb')), callback='nonhumanbooks'),
       Rule(LinkExtractor(allow=('togethernet\-website\.herokuapp.com')), callback='togethernet'),
       Rule(LinkExtractor(allow=('jamesbridle\.com\/works\/autonomous\-trap\-001')), callback='autonomoustrap'),
       Rule(LinkExtractor(allow=('www\.eyebeam\.org\/rapid\-response')), callback='rapidresponse'),
       Rule(LinkExtractor(allow=('torquetorque.net\/publications\/critical-kits')), callback='criticalkits'),
       Rule(LinkExtractor(allow=('autonomous\.mur\.at')), callback='autonomoussystems'),
       Rule(LinkExtractor(allow=('additivism\.org\/about')), callback='additivism'),
       Rule(LinkExtractor(allow=('onursonmez\.com\/home\/sunscriber')), callback='sunscriber'),
       Rule(LinkExtractor(allow=('www\.interface\.ufg\.ac\.at\/christa-laurent\/WORKS\/artworks\/SolarDisplay\/SolarDisplay\.html')), callback='solardisplay'),
       Rule(LinkExtractor(allow=('networksofonesown\.constantvzw\.org\/')), callback='networksofonesown'),
       Rule(LinkExtractor(allow=('solarprotocol\.net\/manifesto\.html')), callback='solarprotocol'),
       Rule(LinkExtractor(allow=('solar\.lowtechmagazine.com\/2020\/01\/how\-sustainable\-is\-a\-solar\-powered\-website\.html')), callback="solarwebsite"), 
       Rule(LinkExtractor(allow=('github\.com\/Organic\-Basics\/ob\-low\-impact\-website')), callback="lowimpactwebsite"), 
    )
    
    # define callback functions here - use css or xpath, make sure to remove HTML with "cleaner.clean_html()""
    # use the inspectori n a browser to get the xpath ("item.xpath()") or the CSS ("item.css()") selector
    # "re.sub" is used to remove or change strings in the HTML
    # "remove_tags()" removes the list of tags completely
    # to get just the text form a specific element add "/text()" to the end of the xpath
    # to get the source of the image att "/@src" at the end of the xpath

    def medialab(self, response):
        for item in response.css('#body'):
            yield {
                'order': "00",
                'title': item.xpath('/html/body/div[3]/section/div/div[1]/div/div[1]/h4/a/text()').get(),
                'content': re.sub('<br><br>MediaLAB', 'MediaLAB', cleaner.clean_html(item.xpath('/html/body/div[3]/section/div/div[1]/div/div[2]/p[5]').get() + item.xpath('/html/body/div[3]/section/div/div[1]/div/div[2]/p[4]').get())),
                'image': "https://hacklab01.org/images/6/8/2/3/8/6823877d352b2e2873caf51c044ee22a6a5ce536-2019-akc-hacklab01-medialab.png",
                'url': response.request.url,
                'date': time.strftime("%Y-%d-%m %H:%M:%S" + " GMT", ts),
            }

    def pivilion(self, response):
        for item in response.css('#main'):
            yield {
                'order': "01",
                'title': 'About Pivilion',
                'content':cleaner.clean_html(remove_tags(item.css('.entry-content').get(), keep=('a','p','strong','ul','ol','li'))),
                'image': item.css('.mosaic::attr(src)').get(),
                'url': response.request.url,
                'date': time.strftime("%Y-%d-%m %H:%M:%S" + " GMT", ts),
            }
    def schlosspost(self, response):
        for item in response.css('#post-5033'):
            yield {
                'order': "02",
                'title': item.css('.page-title::text').get(),
                'content': remove_tags(item.css('p').get()),
                'image': item.css('.taller::attr(src)').get(),
                'url': response.request.url,
                'date': time.strftime("%Y-%d-%m %H:%M:%S" + " GMT", ts),
            }

    def solarsinter(self, response):
        for item in response.css('body'):
            yield {
                'order': "03",
                'title': remove_tags(item.xpath("//*[contains(text(), 'Solar')]").get()),
                'content': item.xpath("//div[@class='body' and .//p[contains(., 'desert manufacturing')]]").get(),
                'image': item.xpath("//img/@src[contains(., 'Solar')]").get(),
                'url': response.request.url,
                'date': time.strftime("%Y-%d-%m %H:%M:%S" + " GMT", ts),
            } 

    def crd(self, response):
        for item in response.css('.art-background'):
            yield {
                'order': "04",
                'title': item.css('.art-text--bold::text').get(),
                'content': re.sub(' style.*\"', '', remove_tags(item.css('.art_interdiv , .art-text--small, p').get(), keep=('a','strong','ul','ol','li'))),
                'image': "https://www.evanmeaney.com/images/artrectangles/recpi/recpi_17.png",
                'url': response.request.url,
                'date': time.strftime("%Y-%d-%m %H:%M:%S" + " GMT", ts),
            }

    def bibliotecha(self, response):
        for item in response.css('.body'):
            yield {
                'order': "05",
                'title': item.css('#bibliotecha-manual::text').get().replace(" Manual", ""),
                'content': item.css('blockquote:nth-child(2) > p:nth-child(2)::text').get(),
                'image': "https://bibliotecha.info/wpdir/wp-content/uploads/2014/04/Bibliotecha_Book_sm_blue30.jpg",
                'url': response.request.url,
                'date': time.strftime("%Y-%d-%m %H:%M:%S" + " GMT", ts),
            }


    def pi2pi(self, response):
        for item in response.xpath('//*[@id="readme"]'):
            yield {
                'order': "06",
                'title': item.xpath('/html/body/div[4]/div/main/div[2]/div/div/div[2]/div[1]/readme-toc/div/div[2]/article/h1[1]/text() ').get(),
                'content': remove_tags(item.xpath('/html/body/div[4]/div/main/div[2]/div/div/div[2]/div[2]/div/div[1]/div/p').get() + item.xpath('/html/body/div[4]/div/main/div[2]/div/div/div[2]/div[1]/readme-toc/div/div[2]/article/p[2]').get(), keep=('p', 'a', 'code', 'strong','ul','ol','li')),
                'image': "https://0100101110101101.org/blog/wp-content/uploads/personal-photographs-exhib-fotomuseum-w-person-MG_0481-1024x683.jpg",
                'url': response.request.url,
                'date': time.strftime("%Y-%d-%m %H:%M:%S" + " GMT", ts),
            }

    def collapseos(self, response):
        for item in response.css('body'):
            yield {
                'order': "07",
                'title': item.css('h1::text').get(),
                'content': '<small>image credit: CC BY 2.0 <a href="https://flic.kr/p/mcZkbj" target="_blank"> Burnt &amp; Abandoned Computer</a> by <a href="https://www.flickr.com/photos/drainrat/" target="_blank">darkday</a></small> <br><br>' + remove_tags(item.css('p:nth-child(3)').get() + item.css('ol').get() + item.css('p:nth-child(5)').get(), keep=('ol', 'li')),
                'image': "https://blog.desdelinux.net/wp-content/uploads/2019/10/collapse-OS.jpg.webp",
                'url': response.request.url,
                'date': time.strftime("%Y-%d-%m %H:%M:%S" + " GMT", ts),
            }
    

    def deaddrops(self, response):
        for item in response.css('body'):
            yield {
                'order': "08",
                'title': item.css('.info h1::text').get(),
                'content': remove_tags(item.xpath('/html/body/div/div/div[2]').get(), keep=('ul', 'p', 'li', 'a')),
                'image': "https://arambartholl.com/wwwppp/wp-content/uploads/2018/03/aram_bartholl_dead_drops_2010_01-e1526384395486-1400x933.jpg",
                'url': response.request.url,
                'date': time.strftime("%Y-%d-%m %H:%M:%S" + " GMT", ts),
            }

    def rpfu(self, response):
        for item in response.css('body'):
            yield {
                'order': "09", 
                'title': item.css('.entry-title::text').get(), 
                'content': cleaner.clean_html(remove_tags(item.xpath('/html/body/div[5]/main/div/div[2]/article/div/div[2]/div/div/div/div[2]/div/p[3]').get() + item.xpath('/html/body/div[5]/main/div/div[2]/article/div/div[2]/div/div/div/div[2]/div/p[4]').get() + item.xpath('/html/body/div[5]/main/div/div[2]/article/div/div[2]/div/div/div/div[2]/div/ul').get(), keep=('p', 'ul', 'li', 'a'))),                  
                'image': 'https://images.squarespace-cdn.com/content/v1/5caa4adb92441be122fcead4/1569891920336-FWFASQM38M229Z1BRGHM/b4.jpg?format=750w',
                'url': response.request.url, 
                'date': time.strftime("%Y-%d-%m %H:%M:%S" + " GMT", ts),
            }
    def towar(self, response):
        for item in response.css('body'):
            yield {
                'order': '10', 
                'title': item.css('.page-title::text').get(), 
                'content': remove_tags(item.xpath('/html/body/div[1]/div[7]/article/p[2]').get(), keep=('ol', 'p', 'li', 'a')),
                'image': "http://sivazona.hr/images/pages/237/Solarni_magarac_567.jpg",
                'url': response.request.url, 
                'date': time.strftime("%Y-%d-%m %H:%M:%S" + " GMT", ts),
            }

    def nonhumanbooks(self, response):
        for item in response.css('body'):
            yield {
                'order': '11',
                'title': item.xpath('/html/body/div/div/div/div/div[2]/div/div[1]/h1/text()').get(),
                'content': remove_tags(item.xpath('/html/body/div/div/div/div/div[2]/div/div[3]/div').get(), keep=('ol', 'p', 'li', 'a')),
                'image': item.xpath('/html/body/div/div/div/div/div[2]/div/div[2]/div/div[1]/img/@src').get(),
                'url': response.request.url,
                'date': time.strftime("%Y-%d-%m %H:%M:%S" + " GMT", ts),
            }
    
    def togethernet(self, response):
        for item in response.css('body'):
            yield {
                'order': '12',
                'title': item.xpath('/html/body/div/div[1]/section/section[1]/h1/text()').get(), 
                'content': remove_tags(item.xpath('/html/body/div/div[1]/section/section[2]').get(), keep=('p')),
                'image': response.request.url + item.xpath('/html/body/div/div[1]/section/section[5]/img/@src').get(),
                'url': response.request.url,
                'date': time.strftime("%Y-%d-%m %H:%M:%S" + " GMT", ts),
            }

    def autonomoustrap(self, response):
        for item in response.css('body'):
            yield {
                'order': '13', 
                'title': item.xpath('/html/body/main/header/h1/text()').get(),
                'content': item.xpath('/html/body/main/div/p[1]').get() + item.xpath('/html/body/main/div/p[2]').get() + item.xpath('/html/body/main/div/p[3]').get(),
                'image': item.xpath('/html/body/main/div/figure[1]/img/@src').get(),
                'url': response.request.url,
                'date': time.strftime("%Y-%d-%m %H:%M:%S" + " GMT", ts),

            }
    
    def rapidresponse(self, response):
        for item in response.css('body'):
            yield {
                'order': '14', 
                'title': 'Rapid Response for A Better Digital Future',
                'content': cleaner.clean_html(remove_tags(item.xpath('/html/body/div[1]/div/ul/li[1]/div/div/h3').get() + item.xpath('/html/body/div[1]/div/ul/li[1]/div/div/div/p[1]').get(), keep=('p'))),
                'image': item.xpath('/html/body/div[1]/div/ul/li[1]/div/figure/a/img/@src').get(),
                'url': response.request.url,
                'date': time.strftime("%Y-%d-%m %H:%M:%S" + " GMT", ts),
            }
    

    def criticalkits(self, response):
        for item in response.css('body'):
            yield {
                'order': '15', 
                'title': 'Critical Kits',
                'content': remove_tags(item.xpath('/html/body/article/div[2]/p').get()),
                'image': 'https://re-dock.org/wp-content/uploads/2017/10/CKTT-cards.jpg',
                'url': response.request.url,
                'date': time.strftime("%Y-%d-%m %H:%M:%S" + " GMT", ts),
            }


    def autonomoussystems(self, response):
        for item in response.css('body'):
            yield {
                'order': '16', 
                'title': 'Autonomous Systems',
                'content': cleaner.clean_html(remove_tags(item.xpath('/html/body/div/div/div/div[3]/main/section/article/div[1]/div/div/section[1]/div/div/div/section/div/div[1]/div/div/div/div').get(), keep=('p'))),          
                'image': item.xpath('/html/body/div/div/div/div[3]/main/section/article/div[1]/div/div/section[1]/div/div/div/div[2]/div/div/img/@src').get(), 
                'url': response.request.url,
                'date': time.strftime("%Y-%d-%m %H:%M:%S" + " GMT", ts),

            }

    def additivism(self, response):
        for item in response.css('body'):
            yield {
                'order': '17', 
                'title': 'The 3D Additivist Manifesto & Cookbook', 
                'content': cleaner.clean_html(item.xpath('/html/body/section/div/div[2]/article/p[1]').get() + item.xpath('/html/body/section/div/div[2]/article/p[3]').get()),
                'image': item.xpath('/html/body/section/div/div[2]/article/p[2]/img/@src').get(),
                'url': response.request.url,
                'date': time.strftime("%Y-%d-%m %H:%M:%S" + " GMT", ts),
            }

    def sunscriber(self, response):
        for item in response.css('body'):
            yield {
                'order': '18', 
                'title': 'Sunscriber', 
                'content': cleaner.clean_html(item.xpath('/html/body/div[5]/div[1]/div/div[2]/div/div/div/div/p[5]').get()),
                'image': 'http://onursonmez.com/home/wp-content/uploads/2021/02/sunscriber_thumbnail-1024x576.jpg',
                'url': response.request.url,
                'date': time.strftime("%Y-%d-%m %H:%M:%S" + " GMT", ts),

            }
    
    def solardisplay(self, response):
        for item in response.css('body'):
            yield {
                'order': '19', 
                'title': 'Solar Display',
                'content': cleaner.clean_html(item.xpath('//td//td//div//p[(((count(preceding-sibling::*) + 1) = 1) and parent::*)]').get()),
                'image': 'http://www.interface.ufg.ac.at/christa-laurent/WORKS/artworks/SolarDisplay/images/SolarDisplay04.jpg',
                'url': response.request.url,
                'date': time.strftime("%Y-%d-%m %H:%M:%S" + " GMT", ts),


            }

    def networksofonesown(self, response):
        for item in response.css('body'):
            yield {
                'order': '20',
                'title': "Networks Of One’s Own",
                'content': re.sub('<a href=\"#fn2\"><sup>2</sup></a>', '', re.sub('<a href=\"#fn1\"><sup>1</sup></a>', '', cleaner.clean_html(item.xpath('/html/body/div/div[1]/p[1]').get() + item.xpath('/html/body/div/div[1]/p[3]').get()))),
                'image': 'https://networksofonesown.varia.zone/three-takes-on-taking-care.png',
                'url': response.request.url,
                'date': time.strftime("%Y-%d-%m %H:%M:%S" + " GMT", ts),

            }

    def solarprotocol(self, response):
        
        for item in response.css('body'): 
            yield {            
                'order': '21', 
                'title': 'Solar Protocol', 
                'content': "content goes here", 
                'image': 'http://solarprotocol.net/images/earth.gif', 
                'url': response.request.url,
                'date': time.strftime("%Y-%d-%m %H:%M:%S" + " GMT", ts),
            }

    def solarwebsite(self, response):
        for item in response.css('body'): 
            yield {
                'order': '22',
                'title': 'How Sustainable is a Solar Powered Website?',
                'content': cleaner.clean_html(item.xpath('/html/body/main/section[1]/div[1]/p[13]').get() + item.xpath('/html/body/main/section[1]/div[1]/p[14]').get()),
                'image': 'https://solar.lowtechmagazine.com/dithers/detail-system-diego.png', 
                'url': response.request.url,
                'date': time.strftime("%Y-%d-%m %H:%M:%S" + " GMT", ts),
            }

    def lowimpactwebsite(self, response):
        for item in response.css('body'):
            yield {
                'order': '23',
                'title': 'The Low Impact Website',
                'content': item.xpath('/html/body/div[4]/div/main/div[2]/div/div/div[2]/div[1]/readme-toc/div/div[3]/article/p[2]').get() + item.xpath('/html/body/div[4]/div/main/div[2]/div/div/div[2]/div[2]/div/div[1]/div/div[1]/span').get(),
                'image': 'https://raw.githubusercontent.com/Organic-Basics/ob-low-impact-website/develop/assets/svg/manifesto/manifesto_1.svg',
                'url': response.request.url,
                'date': time.strftime("%Y-%d-%m %H:%M:%S" + " GMT", ts),
            }

# setup the json file where the data will be saved

process = CrawlerProcess(settings={
    'FEED_FORMAT': 'json',
    'FEED_URI': 'pivilion-unsorted.json'
})

process.crawl(PiSpider)
process.start()

# sort the unsorted items in the order we defined in the functions
 
f = open('pivilion-unsorted.json',)
data = json.load(f)
data.sort(key=operator.itemgetter('order'))
print(json.dumps(data, indent=2))
f.close()

# save the sorted items as a json file

with open('pivilion.json', 'w') as outfile:
    json.dump(data, outfile)

# delete the unsorted file 

os.remove("pivilion-unsorted.json")

# make static html from the json file and put into the html/pivilion.html file

f = open('pivilion.json',)
data = json.load(f)
i=0
out = open("/var/www/html/static/index.html", "w")
print(

'''
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" media="all" href="css.css" type="text/css" />
    <title>Pivilion · Notes on Autonomy · mur.at</title>
    <style>
        img[src*="none"] {
        display: none;
        }
    </style>

</head>
<body>
    <div id="book">
    <div id="first">
    <h1>Notes on Autonomy</h1>
    <h4>Generated on '''
    
, file=out)
print(time.strftime("%Y-%d-%m %H:%M:%S" + " GMT", ts), file=out)
print(
'''
    </h4>
    </div>
    <div class="spacer-page"></div>
    <section class="toc">
    <h1 class="hidden">Table of Contents</h1>
'''
, file=out)

#generate toc

i=0
while(i < len(data)):
    print('<div class="toc-row">' +'\n' + '<a class="toc-num" href="#title' + data[i]["order"] +'">' + '</a>' + '\n' + '<a class="toc-title" href="#title' + data[i]["order"] + '">' + data[i]["title"] + '</a>' + '</div>' '\n', file=out)
    i += 1
print('</section>', file=out)

#generate content
i=0
while(i < len(data)):
      print('<h1' + ' id="title' + data[i]["order"] + '">' + data[i]["title"] + '</h1>' + '<img src="' + data[i]["image"] + '"/>' + '<div class="content"><p>' + data[i]["content"] + '</p>' + '<h5>Retrieved from ' + data[i]["url"] + '</h5>' + '\n' + '</div>', file=out)
      i += 1

print(
    '''
</body>
</html>
    '''
, file=out)
