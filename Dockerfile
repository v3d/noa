# set base image (host OS)
FROM python:3.9.6-slim-bullseye

# set the working directory in the container
WORKDIR /pivilion

# copy the dependencies file to the working directory
COPY requirements.txt .

# copy cron file
COPY cron /etc/cron.d/python-script

# upgrade pip, install deps, remove default nginx
 
RUN pip install --upgrade pip \
    && pip install -r requirements.txt \
    && apt-get update && apt-get install nginx cron -y \
    && echo "daemon off;" >> /etc/nginx/nginx.conf \
    && chmod 0744 /etc/cron.d/python-script \
    && crontab /etc/cron.d/python-script

#expose port 80 to the host
EXPOSE 80

# command to run on container start
CMD chmod 0744 /files/scripts/scraper.py && rm -rf /files/scripts/pivilion-unsorted.json || true && cron && service nginx start && python /files/scripts/scraper.py
 
